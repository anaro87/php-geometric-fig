<?php

// /////////////////////////////////////////////////////////////////////////////
// WORKING AREA
// THIS IS AN AREA WHERE YOU SHOULD WRITE YOUR CODE AND MAKE CHANGES
// /////////////////////////////////////////////////////////////////////////////

namespace App;

use App\Circle;
use App\Exceptions\UnsupportedShapeException;

/**
 * Class ShapeFactory
 * @package App
 */
use App\Exceptions\WrongParamCountException;
use App\Rectangle;
use App\Square;

class ShapeFactory {
	/**
	 * Creates a specific GeometricShape object from the given attributes.
	 *
	 * Usage examples:
	 *     ShapeFactory::createShape("Circle", 4)
	 *     ShapeFactory::createShape("Rectangle", [3, 5])
	 *
	 * @param string $shape
	 * @param array $params
	 * @return mixed
	 * @throws WrongParamCountException|UnsupportedShapeException
	 */
	public static function createShape(string $shape, array $params = []) 
    {
		switch ($shape) {
		case "Circle":
			if (count($params) != 1) {
				throw new WrongParamCountException('Wrong Param Count');
			}
			$r = $params[0];
			$circle = new Circle($r);
			return $circle;
		case "Rectangle":
			if (count($params) != 2) {
				throw new WrongParamCountException('Wrong Param Count');
			}
			$width = $params[0];
			$height = $params[1];
			$rectangle = new Rectangle($width, $height);
			return $rectangle;
		case "Square":
			if (count($params) != 1) {
				throw new WrongParamCountException('Wrong Param Count');
			}
			$edgeSize = $params[0];
			$square = new Square($edgeSize);
			return $square;

		default:
			throw new UnsupportedShapeException('Unsupported Shape');
			break;
		}
	}
}